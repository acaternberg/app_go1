#! /bin/bash

#go get github.com/stretchr/testify/assert
#go mod init github.com/stretchr/testify/assert

#Install https://github.com/gotestyourself/gotestsum
#export PATH=$PATH:/Users/andreascaternberg/go/bin
go mod init gotest.tools/gotestsum
go test main_test.go | gotestsum --format testname --junitfile unit-tests.xml
go test main_test.go -cover
