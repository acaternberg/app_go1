#! /bin/bash
#see https://blog.stack-labs.com/code/kustomize-101/

#Example: kustomize.sh caternberg/helloworldgolang-1-a984b6020be37368413d013908fa31af450f4d06

IMAGE=${1:-'caternberg/helloworldgolang-1-a984b6020be37368413d013908fa31af450f4d06'}

#This is the imagename template inside k8s/base/deployment.yaml
#DEFAULT_IMAGE and the imagename inside deployment.yaml must be the same.
#If not, kustomize can not replcae it!
DEFAULT_IMAGE=helloworldgolang

#This is the service name where we are getting the external IP from later on
SERVICE=helloworld-service

#kustomize edit set image  is bugy
#https://www.npmjs.com/package/kustomize-edit-set-image
#https://github.com/kubernetes-sigs/kustomize/issues/2475
#So, we need to workaround
cat << EOF >> k8s/base/kustomization.yaml
images:
  - name: ${DEFAULT_IMAGE}
    newName: ${IMAGE}
    #newTag: ""
EOF

#install kustomize
#curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash

#agregate and show k8s yaml deploy resource
kustomize build k8s/overlays/dev

#delete previoues deployment
kustomize build k8s/overlays/dev | kubectl delete -f -

#apply deployment
kustomize build k8s/overlays/dev | kubectl apply -f -

#wait until service external IP is published
SERVICE_IP=""
while [ -z "$SERVICE_IP" ]
do
    echo "SERVICE_IP is pending, wait........"
    SERVICE_IP=$(kubectl get service/${SERVICE} -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
    SERVICE_IP=$(echo $SERVICE_IP | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}")
    [ -z "$SERVICE_IP" ] && sleep 30
done
echo "Service external IP is created SERVICE_IP: $SERVICE_IP"
kubectl get all

#run curl against service
curl -Lvk http://$SERVICE_IP:8666

#check for http state 200
curl -sI http://$SERVICE_IP:8666 | grep -o "HTTP/1.1 200 OK"

